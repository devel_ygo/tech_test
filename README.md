# tech_test

Prueba Técnica.
# Muy Importante, se requiere internet para que las vistas de usuario mantengan con el estilo bootstrap.
# La aplicación se desarrollo con:
# Java.
# Spring Boot.
# MySql.
# thymeleaf.
# bootstrap.
#
# El acceso principal esta bajo el recurso Url:
# http://localhost:8080
# Posteriormente, se presentará una lista con las opciones disponibles.
# see institute.
# Recuros que responde a la gestión de la clase entity y tabla institute, se puede listar los registros y editar.
# http://localhost:8080/institute/index
# see institute type.
# Recuros que responde a la gestión de la clase entity y tabla institute_type, se puede listar.
# http://localhost:8080/type_institute/index
# see location institute type.
# Recuros que responde a la gestión de la clase entity y tabla location_institute_type, se puede listar.
# http://localhost:8080/loc_element_types/index

# El motor de base datos MySql requiere de los siguientes parametros:
# Nota: La base de datos donde se desarrollo la aplicación se encontraba en un Docker, es por ello que tiene definido un puerto y su host diferente.
# Spring.datasource.url=jdbc:mysql://127.0.0.1:33060/tech_test
# Spring.datasource.username=demo
# Spring.datasource.password=
# Spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
# 
# Spring.jpa.show-sql=true
# spring.jpa.hibernate.ddl-auto=update
# spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect

# En el directorio de los recursos se encuentra el script de la base de datos.
# .\tech_test\tech_test\src\main\resources\sql 