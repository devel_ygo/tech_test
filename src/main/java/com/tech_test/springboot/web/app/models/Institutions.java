package com.tech_test.springboot.web.app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "institutions")
public class Institutions {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String acronym;
	private String website_link;
	private Integer program_id;
	private Integer institution_type_id; 
	private String added;

	public Institutions() {
	}

	public Institutions(int id, String name, String acronym, String website_link, Integer program_id,
			Integer institution_type_id, String added) {
		super();
		this.id = id;
		this.name = name;
		this.acronym = acronym;
		this.website_link = website_link;
		this.program_id = program_id;
		this.institution_type_id = institution_type_id;
		this.added = added;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getWebsite_link() {
		return website_link;
	}

	public void setWebsite_link(String website_link) {
		this.website_link = website_link;
	}

	public Integer getProgram_id() {
		return program_id;
	}

	public void setProgram_id(Integer program_id) {
		this.program_id = program_id;
	}

	public Integer getInstitution_type_id() {
		return institution_type_id;
	}

	public void setInstitution_type_id(Integer institution_type_id) {
		this.institution_type_id = institution_type_id;
	}

	public String getAdded() {
		return added;
	}

	public void setAdded(String added) {
		this.added = added;
	}

	

}
