package com.tech_test.springboot.web.app.interfaceServices;

import java.util.List;
import java.util.Optional;

import com.tech_test.springboot.web.app.models.Institutions;

public interface IInstitution_Services {

	public List<Institutions> Listar();
	
	public Optional<Institutions> listarId(int id);
	
	public int guardar (Institutions institutions);
}
