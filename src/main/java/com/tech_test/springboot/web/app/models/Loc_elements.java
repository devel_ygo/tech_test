package com.tech_test.springboot.web.app.models;

public class Loc_elements {

	private Integer id;
	private String name;
	private String iso_alpha_2;
	private Integer parent_id;
	private Integer element_type_id;
	private Integer is_active;
	public Loc_elements() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIso_alpha_2() {
		return iso_alpha_2;
	}
	public void setIso_alpha_2(String iso_alpha_2) {
		this.iso_alpha_2 = iso_alpha_2;
	}
	public Integer getParent_id() {
		return parent_id;
	}
	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}
	public Integer getElement_type_id() {
		return element_type_id;
	}
	public void setElement_type_id(Integer element_type_id) {
		this.element_type_id = element_type_id;
	}
	public Integer getIs_active() {
		return is_active;
	}
	public void setIs_active(Integer is_active) {
		this.is_active = is_active;
	}
	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

}
