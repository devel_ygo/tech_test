package com.tech_test.springboot.web.app.interfaceServices;

import java.util.List;
import java.util.Optional;

import com.tech_test.springboot.web.app.models.Institution_types;

public interface Iinstitution_types_Service {

	public List<Institution_types> Listar();
	
	public Optional<Institution_types> listarId();
	
	public int guardar(Institution_types inst_type);
	
	public void borrar (int id);
}
