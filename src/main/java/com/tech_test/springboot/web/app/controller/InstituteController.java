package com.tech_test.springboot.web.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech_test.springboot.web.app.models.Institutions;
import com.tech_test.springboot.web.app.services.Institutions_Services;

@Controller
@RequestMapping("/institute")
public class InstituteController {
	@Value("${text.InstituteController.index.titlpage}")
	private String titlPage;

	@Value("${text.InstituteController.index.titlenca}")
	private String titlenca;

	@Value("${text.loc_element_typesController.index.titldeta}")
	private String titldeta;

	@Autowired
	private Institutions_Services institution_Services;

	/***
	 * Metodo con el cual declaramos un titulo para todo la clase controller
	 * 
	 * @return El titulo de la barra
	 */
	@ModelAttribute("titlBarr")
	public String tituloBarra() {
		return titlPage;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titlenca")
	public String textoEnca() {
		return titlenca;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titldeta")
	public String textoDeta() {
		return titldeta;
	}

	/***
	 * Metodo con el atributo Model
	 * 
	 * @param model Con el cual se agregan los datos que serán visto en la página,
	 *              sus valores son asignados en par de forma [clave, valor]
	 * @return El nombre de la página web o vista .
	 */
	@GetMapping({ "/", "", "home" })
	/* @RequestMapping(value = "/index", method=RequestMethod.GET) */
	public String index(Model model) {
		return "index";
	}

	/***
	 * Metodo con el cual consultamos las instituciones Este metodo me llevara a la
	 * página index.html para la cual me mostrará las opciones para enviar
	 * parametros por la Url.
	 * 
	 * @return Nombre de la página index del controller.
	 */
	@GetMapping("/index")
	public String type_institute(Model model) {
		List<Institutions> listObje;
		try {
			listObje = institution_Services.Listar();
			model.addAttribute("items", listObje);
			return "institute";
		} catch (NullPointerException e) {
			listObje = null;
			return "institute";
		}
	}

	/***
	 * página index.html para la cual me mostrará las opciones para enviar
	 * parametros por la Url.
	 * 
	 * @return Nombre de la página index del controller.
	 */
	@GetMapping("/new")
	public String addInstitute(Model model) {
		model.addAttribute("items", new Institutions());
		return "form";
	}

	@PostMapping("/save")
	public String save(@Validated Institutions institutions, Model model) {
		try {
			institution_Services.guardar(institutions);
			return "redirect:/institute/index";
		} catch (NullPointerException e) {
			return "institute";
		}
	}

	@GetMapping("/edit/{id}")
	public String editInstitute(@PathVariable int id, Model model) {
		Optional<Institutions> institution = institution_Services.listarId(id);
		model.addAttribute("items", institution);
		return "form";
	}

}
