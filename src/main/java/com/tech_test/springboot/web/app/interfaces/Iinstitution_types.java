package com.tech_test.springboot.web.app.interfaces;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tech_test.springboot.web.app.models.Institution_types;

@Repository
public interface Iinstitution_types extends CrudRepository<Institution_types, Integer>{

}
