package com.tech_test.springboot.web.app.models;

public class Institutions_locations {

	private Integer id;
	private Integer institution_id;
	private Integer loc_element_id;
	private Integer is_headquater;
	private String city;
	
	public Institutions_locations () {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getInstitution_id() {
		return institution_id;
	}

	public void setInstitution_id(Integer institution_id) {
		this.institution_id = institution_id;
	}

	public Integer getLoc_element_id() {
		return loc_element_id;
	}

	public void setLoc_element_id(Integer loc_element_id) {
		this.loc_element_id = loc_element_id;
	}

	public Integer getIs_headquater() {
		return is_headquater;
	}

	public void setIs_headquater(Integer is_headquater) {
		this.is_headquater = is_headquater;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	
}
