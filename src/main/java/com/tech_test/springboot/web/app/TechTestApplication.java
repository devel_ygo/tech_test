package com.tech_test.springboot.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class TechTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechTestApplication.class, args);
	}

}
