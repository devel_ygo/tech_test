package com.tech_test.springboot.web.app.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tech_test.springboot.web.app.models.Institutions;

@Repository
public interface IInstitutions extends CrudRepository<Institutions, Integer> {

}
