package com.tech_test.springboot.web.app.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tech_test.springboot.web.app.interfaceServices.ILoc_element_types_Services;
import com.tech_test.springboot.web.app.interfaces.ILoc_element_types;
import com.tech_test.springboot.web.app.models.Loc_element_types;

@Service
public class Loc_element_types_Services implements ILoc_element_types_Services {

	@Autowired
	private ILoc_element_types services;
	
	@Override
	public List<Loc_element_types> Listar() {
		// TODO Auto-generated method stub
		return (List<Loc_element_types>) services.findAll();
	}

	@Override
	public Optional<Loc_element_types> listarId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int guardar(Loc_element_types institutions) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void borrar(int id) {
		// TODO Auto-generated method stub
		
	}

}
