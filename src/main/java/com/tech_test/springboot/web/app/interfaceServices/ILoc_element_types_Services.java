package com.tech_test.springboot.web.app.interfaceServices;

import java.util.List;
import java.util.Optional;

import com.tech_test.springboot.web.app.models.Loc_element_types;

public interface ILoc_element_types_Services {
	public List<Loc_element_types> Listar();

	public Optional<Loc_element_types> listarId();

	public int guardar(Loc_element_types institutions);

	public void borrar(int id);
}
