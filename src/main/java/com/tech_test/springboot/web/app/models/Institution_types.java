package com.tech_test.springboot.web.app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "institution_types")
public class Institution_types {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String acronym;
	private String rep_ind_organization_type_id;

	public Institution_types() {
	}

	public Institution_types(int id, String name, String acronym, String rep_ind_organization_type_id) {
		this.id = id;
		this.name = name;
		this.acronym = acronym;
		this.rep_ind_organization_type_id = rep_ind_organization_type_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getRep_ind_organization_type_id() {
		return rep_ind_organization_type_id;
	}

	public void setRep_ind_organization_type_id(String rep_ind_organization_type_id) {
		this.rep_ind_organization_type_id = rep_ind_organization_type_id;
	}

	
}
