package com.tech_test.springboot.web.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech_test.springboot.web.app.interfaceServices.Iinstitution_types_Service;
import com.tech_test.springboot.web.app.models.Institution_types;

@Controller
@RequestMapping("/type_institute")
public class TypeInstituteController {
	@Value("${text.TypeInstituteController.index.titlpage}")
	private String titlPage;

	@Value("${text.TypeInstituteController.index.titlenca}")
	private String titlenca;

	@Value("${text.TypeInstituteController.index.titldeta}")
	private String titldeta;

	@Autowired
	private Iinstitution_types_Service institution_types_services;
	/***
	 * Metodo con el cual declaramos un titulo para todo la clase controller
	 * 
	 * @return El titulo de la barra
	 */
	@ModelAttribute("titlBarr")
	public String tituloBarra() {
		return titlPage;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titlenca")
	public String textoEnca() {
		return titlenca;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titldeta")
	public String textoDeta() {
		return titldeta;
	}

	/***
	 * Metodo con el atributo Model
	 * 
	 * @param model Con el cual se agregan los datos que serán visto en la página,
	 *              sus valores son asignados en par de forma [clave, valor]
	 * @return El nombre de la página web o vista .
	 */
	@GetMapping({ "/", "", "home" })
	/* @RequestMapping(value = "/index", method=RequestMethod.GET) */
	public String index(Model model) {
		return "index";
	}

	/***
	 * Metodo con el cual consultamos los tipos de instituciones. Este metodo me
	 * llevara a la página index.html para la cual me mostrará las opciones para
	 * enviar parametros por la Url.
	 * 
	 * @return Nombre de la página index del controller.
	 */
	@GetMapping("/index")
	public String type_institute(Model model) {
		List<Institution_types> listObje;
		try {
			listObje = institution_types_services.Listar();
			model.addAttribute("institution_type", listObje);
			return "type_institute";
		} catch (NullPointerException e) {
			listObje = null;
			return "type_institute";
		}
	}
}
