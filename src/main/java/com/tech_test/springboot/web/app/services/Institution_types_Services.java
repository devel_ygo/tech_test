package com.tech_test.springboot.web.app.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tech_test.springboot.web.app.interfaceServices.Iinstitution_types_Service;
import com.tech_test.springboot.web.app.interfaces.Iinstitution_types;
import com.tech_test.springboot.web.app.models.Institution_types;

@Service
public class Institution_types_Services implements Iinstitution_types_Service{

	@Autowired
	private Iinstitution_types Institution_types_Servives;
	
	@Override
	public List<Institution_types> Listar() {
		// TODO Auto-generated method stub
		return (List<Institution_types>) Institution_types_Servives.findAll();
	}

	@Override
	public Optional<Institution_types> listarId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int guardar(Institution_types inst_type) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void borrar(int id) {
		// TODO Auto-generated method stub
		
	}

}
