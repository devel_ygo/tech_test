package com.tech_test.springboot.web.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech_test.springboot.web.app.interfaceServices.ILoc_element_types_Services;
import com.tech_test.springboot.web.app.models.Loc_element_types;

@Controller
@RequestMapping("/loc_element_types")
public class Loc_element_typesController {
	@Value("${text.loc_element_typesController.index.titlpage}")
	private String titlPage;

	@Value("${text.loc_element_typesController.index.titlenca}")
	private String titlenca;

	@Value("${text.loc_element_typesController.index.titldeta}")
	private String titldeta;

	@Autowired
	private ILoc_element_types_Services Loc_element_types_Services;

	/***
	 * Metodo con el cual declaramos un titulo para todo la clase controller
	 * 
	 * @return El titulo de la barra
	 */
	@ModelAttribute("titlBarr")
	public String tituloBarra() {
		return titlPage;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titlenca")
	public String textoEnca() {
		return titlenca;
	}

	/***
	 * Metodo con el cual declaramos un texto del cuerpo de la clase controller.
	 * 
	 * @return El texto de la clase controller.
	 */
	@ModelAttribute("titldeta")
	public String textoDeta() {
		return titldeta;
	}

	/***
	 * Metodo con el atributo Model
	 * 
	 * @param model Con el cual se agregan los datos que serán visto en la página,
	 *              sus valores son asignados en par de forma [clave, valor]
	 * @return El nombre de la página web o vista .
	 */
	@GetMapping({ "/", "", "home" })
	/* @RequestMapping(value = "/index", method=RequestMethod.GET) */
	public String index(Model model) {
		return "index";
	}

	/***
	 * Metodo con el cual consultamos las instituciones Este metodo me llevara a la
	 * página index.html para la cual me mostrará las opciones para enviar
	 * parametros por la Url.
	 * 
	 * @return Nombre de la página index del controller.
	 */
	@GetMapping("/index")
	public String type_institute(Model model) {
		List<Loc_element_types> listObje;
		try {
			listObje = Loc_element_types_Services.Listar();
			model.addAttribute("items", listObje);
			return "loc_element_types";
		} catch (NullPointerException e) {
			listObje = null;
			return "loc_element_types";
		}
	}

}
