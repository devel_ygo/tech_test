package com.tech_test.springboot.web.app.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tech_test.springboot.web.app.interfaceServices.IInstitution_Services;
import com.tech_test.springboot.web.app.interfaces.IInstitutions;
import com.tech_test.springboot.web.app.models.Institutions;

@Service
public class Institutions_Services implements IInstitution_Services {

	@Autowired
	private IInstitutions iInstitute;

	@Override
	public List<Institutions> Listar() {
		// TODO Auto-generated method stub
		return (List<Institutions>) iInstitute.findAll();
	}

	@Override
	public Optional<Institutions> listarId(int id) {
		// TODO Auto-generated method stub
		return iInstitute.findById(id);
	}

	@Override
	public int guardar(Institutions institutions) {
		Institutions itemInstitute = iInstitute.save(institutions);
		if (!itemInstitute.equals(null)) {
			return 1;
		}
		return 0;
	}

}
