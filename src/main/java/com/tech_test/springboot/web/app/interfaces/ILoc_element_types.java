package com.tech_test.springboot.web.app.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tech_test.springboot.web.app.models.Loc_element_types;

@Repository
public interface ILoc_element_types extends CrudRepository<Loc_element_types, Integer>{

}
